from collections import deque
from time import time


N_MEANSUREMENTS = 50


class FPSController:
    def __init__(self, main_app):
        """
        The class for measuring the processing time of
        a single frame by a neural network
        """
        self.app = main_app
        self.last_n_times = deque(maxlen=10)
        self.start_time = 0
        self.note_counter = 0

        config = (self.app.config.show_detection_video,
                  self.app.config.show_live_video,
                  self.app.config.terminal_output)
        self.config = "-".join(map(lambda i: str(i)[0], config))

        with open(f"FrameProcessTime_{self.config}.txt", "w") as f:
            f.write("Test frame process time\n")
            f.write("Detection video / Live video / Terminal output\n")
            f.write(f"{config}\n")

    def start_measurement(self):
        """
        This method is called before commands
        whose execution time needs to be measured
        """
        self.start_time = time()

    def end_measurement(self):
        """
        This method is called after commands
        whose execution time needs to be measured
        """
        process_time = time() - self.start_time
        self.last_n_times.append(process_time)

    @property
    def process_time(self) -> int:
        """
        This method calculates the average value
        of 10 measurements and outputs the result
        """
        process_time_count = len(self.last_n_times)
        if process_time_count > 0:
            return sum(self.last_n_times) / process_time_count
        return 0

    def save_process_time(self):
        """
        This method saves the results of N_MEANSUREMENTS measurements to a file
        """
        if self.note_counter < N_MEANSUREMENTS:
            with open(f"FrameProcessTime_{self.config}.txt", "a+") as f:
                f.write(f"Frame process in"
                        f"{self.process_time * 1000:.2f} ms\n")
            self.note_counter += 1
