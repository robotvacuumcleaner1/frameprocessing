import sys
import cv2
from time import sleep
import keyboard
import numpy as np
from sources.config import Config
from sources.Detection import Detection, DetectionData
from sources.GPSDataGetter import GPSDataGetter
from sources.DataSaver import DataSaver
from sources.Activator import GUIActivator
from sources.FPS_controller import FPSController


class DroneCamera:
    def __init__(self):
        self.config = Config("config.ini")
        self.video = cv2.VideoCapture(self.config.camera)
        if self.video is None or not self.video.isOpened():
            print("Camera not found")
            sys.exit()
        # self.video.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        # self.video.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        self.detection = Detection(self)
        self.gps = GPSDataGetter(self)
        self.saver = DataSaver(self)
        self.fps = FPSController(self)
        self.is_run = True
        keyboard.add_hotkey("ctrl+c", self.exit_hotkey_handler)

    def main(self):
        try:
            gui = GUIActivator(self)
            if gui.subscription_is_active():
                sleep(2)
                print("DroneCamera start\nPress 'Ctrl+C' to stop")
                self.run()
        except cv2.error:
            # print("000", e, "000")
            print("Camera connection lost")
        finally:
            cv2.destroyAllWindows()
            self.video.release()
            self.saver.save()

    def run(self):
        self.gps.start()
        self.detection.start()
        detection_frame = None
        while self.is_run:
            is_read, frame = self.video.read()

            if detection_frame is None:
                detection_frame = np.zeros((frame.shape), dtype=np.uint8)

            if not is_read:
                print("Frame read fail")
                self.is_run = False
                break

            if frame is None:
                print("Frame is None")
                self.is_run = False
                break

            detection_result = self.detection.result
            if detection_result is not None:
                detection_frame = self.add_text_to_frame(detection_result)

                self.saver.add_detection_note(detection_result)
                if self.config.terminal_output:
                    output_class = detection_result.detection_class
                    gps_coordinates = detection_result.gps
                    print("\nDetection:\n"
                          f"\tdetected class: {output_class}\n"
                          f"\tcoordinates: {gps_coordinates}")
                self.detection.result = None

            if self.detection.task is None:
                self.detection.task = DetectionData(frame=frame,
                                                    detection_class=None,
                                                    gps=self.gps.coordinates)

            self.show_output_video(frame, detection_frame)

        print("DroneCamera stop")
        sys.exit(0)

    def add_text_to_frame(self, data: DetectionData) -> np.ndarray:
        """
        This method add text with detection result for last detection frame

        :params numpy.ndarray data: The result from detection:
        """
        frame = data.frame
        font = cv2.FONT_HERSHEY_PLAIN

        rectangle_color = (255, 255, 255, 128)
        cv2.rectangle(frame, (0, 0), (550, 80),
                      rectangle_color, thickness=cv2.FILLED)
        cv2.putText(frame, data.detection_class, (10, 30),
                    font, 1.5, (0, 0, 0), 2, cv2.LINE_AA)
        cv2.putText(frame, f"{data.gps}", (10, 60),
                    font, 1.5, (0, 0, 0), 2, cv2.LINE_AA)
        return frame

    def show_output_video(self,
                          frame: np.ndarray,
                          detection_frame: np.ndarray) -> None:
        """
        This method output GUI video according to config settings

        :params numpy.ndarray frame: The frame of raw input videostream:
        :params numpy.ndarray frame: The frame with last detection:
        """

        if self.config.show_live_video or self.config.show_detection_video:
            if (self.config.show_live_video and
                    self.config.show_detection_video):
                output = np.concatenate((frame, detection_frame), axis=1)
            elif self.config.show_live_video:
                output = frame
            elif self.config.show_detection_video:
                output = detection_frame

            output = self.scale_image(output)
            cv2.imshow("Output", output)

            key = cv2.waitKey(1) & 0xFF
            if key == ord("q"):
                self.is_run = False
            if cv2.getWindowProperty("Output", cv2.WND_PROP_VISIBLE) < 1:
                self.is_run = False

    def scale_image(self, frame: np.ndarray) -> np.ndarray:
        """
        This method scale output image for view

        :params numpy.ndarray data: The result from detection:
        """
        h, w = frame.shape[0:2]
        new_h = 480
        new_w = int(new_h * (w / h))
        img = cv2.resize(frame, (new_w, new_h))
        return img

    def exit_hotkey_handler(self):
        """
        This method provide carefull finish program by 'Ctrl+c' hotkey
        """
        self.is_run = False
