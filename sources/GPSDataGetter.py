from gpsdclient import GPSDClient
import threading


class GPSDataGetter(threading.Thread):
    def __init__(self, main_app):
        super().__init__(name="gps-data-getter-thread", daemon=True)
        self.app = main_app
        self._coordinates = {"lat": "n/a", "lon": "n/a"}

    def run(self):
        with GPSDClient() as client:
            for gps in client.dict_stream(convert_datetime=True,
                                          filter=["TPV"]):
                self._coordinates = {"lat": gps.get("lat", "n/a"),
                                     "lon": gps.get("lon", "n/a")}
                if not self.app.is_run:
                    print("gps stop")
                    break

    @property
    def coordinates(self):
        """
        This method return current coordinates
        """
        return self._coordinates
