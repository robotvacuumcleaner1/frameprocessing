#!/bin/bash

# Активировать виртуальное окружение
source ./env/bin/activate

# Запустить Python-приложение
sudo ./env/bin/python3 main.py
