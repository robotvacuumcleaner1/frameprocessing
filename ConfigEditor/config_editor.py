from tkinter import (Entry, Label, Tk, Menu, filedialog,
                     messagebox, LabelFrame, StringVar)
import tkinter as tk
from scroll_frame import ScrollFrame
from about_dialog import AboutDialog
from configparser import ConfigParser, Error
from copy import deepcopy


class ConfigEditor(Tk):
    """
    A class that implements a GUi window for editing a configuration ini file
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.title("ConfigEditor")
        self.geometry("640x480")
        self.minsize(640, 480)
        self.main_menu = Menu(self)
        self.config(menu=self.main_menu)
        self.protocol("WM_DELETE_WINDOW", self.close_window)

        self.edit_config = None
        self.save_config = None

        self.text_variables = []
        self.text_var_iterator = None

        file_menu = Menu(self.main_menu, tearoff=0)
        file_menu.add_command(label="Открыть", command=self.open_config_file)
        file_menu.add_command(label="Сохранить", command=self.save_config_file)

        about_menu = Menu(self.main_menu, tearoff=0)
        about_menu.add_command(label="О программе", command=self.show_about)

        self.main_menu.add_cascade(label="Файл", menu=file_menu)
        self.main_menu.add_cascade(label="Справка", menu=about_menu)

        self.scroll_frame = ScrollFrame(self)
        self.scroll_frame.pack(fill=tk.BOTH, expand=True)

    def close_window(self) -> None:
        """
        Redefined window closing function to check
        for unsaved changes before closing a file
        """

        if self.__have_unsaved_changes():
            if messagebox.askyesno("Выход",
                                   "У вас есть несохранённые изменения "
                                   "конфигурации, сохранить?"):
                self.save_config_file()
        self.destroy()

    def open_config_file(self) -> None:
        """Function to open the configuration file for editing"""

        path = filedialog.askopenfilename(filetypes=[("Config files", "*.ini"),
                                                     ("All files", "*.*")])
        config = ConfigParser()
        try:
            config.read(path)
        except (Error, UnicodeDecodeError) as error:
            messagebox.showerror("Ошибка",
                                 "Ошибка чтения файла конфигурации\n"
                                 f"{error}")
        else:
            if path:
                if self.__have_unsaved_changes():
                    if messagebox.askyesno("Выход",
                                           "У вас есть несохранённые "
                                           "изменения конфигурации, "
                                           "сохранить?"):
                        self.save_config_file()
                    self.fill_window(config)
                else:
                    self.fill_window(config)

    def fill_window(self, config: ConfigParser) -> None:
        """
        The function of filling the window with
        the contents of the configuration file
        """

        self.scroll_frame.clear()
        self.edit_config = config
        self.save_config = deepcopy(config)
        self.__set_window_vars(self.scroll_frame.scrollable_frame,
                               self.edit_config._sections)

    def save_config_file(self) -> None:
        """Function to save the configuration file after changes"""

        if self.edit_config is not None:
            self.__get_window_vars(self.edit_config._sections)
            self.text_var_iterator = None
            path = filedialog.asksaveasfilename(
                defaultextension=".ini",
                filetypes=[("Config files", "*.ini"),
                           ("All files", "*.*")])
            if path:
                try:
                    with open(path, 'w') as f:
                        self.edit_config.write(f)
                except (FileNotFoundError,
                        PermissionError,
                        IOError,
                        OSError) as error:
                    messagebox.showerror("Ошибка",
                                         "Ошибка сохранения"
                                         "файла конфигурации\n"
                                         f"{error}")
                else:
                    messagebox.showinfo("Успех",
                                        "Файл конфигурации успешно сохранён")
                    self.save_config = deepcopy(self.edit_config)

    def __set_window_vars(self, parent, config: ConfigParser) -> None:
        """
        The function of recursively traversing the
        configuration dictionary to create widgets
        """

        row = 0
        for section, value in config.items():
            if isinstance(value, dict):
                section_frame = LabelFrame(parent, text=section)
                section_frame.pack(fill=tk.BOTH, expand=True, padx=10, pady=10)
                section_frame.columnconfigure(0, weight=1)
                section_frame.columnconfigure(1, weight=2)
                self.__set_window_vars(section_frame, config[section])
            else:
                text_var = StringVar(value=value)
                self.text_variables.append(text_var)
                label = Label(parent, text=section)
                entry = Entry(parent, width=40, textvariable=text_var)
                label.grid(row=row, column=0, sticky=tk.W, padx=5, pady=5)
                entry.grid(row=row, column=1, sticky=tk.E, padx=5, pady=5)
                row += 1

    def __get_window_vars(self, config: ConfigParser) -> None:
        """
        The function of recursively traversing the
        configuration dictionary to get the current widget values
        """

        for section, value in config.items():
            if isinstance(value, dict):
                self.__get_window_vars(config[section])
            else:
                config[section] = self.__get_text_variable()

    def __have_unsaved_changes(self) -> bool:
        """
        The function of checking for unsaved changes
        """

        if self.edit_config and self.save_config:
            self.__get_window_vars(self.edit_config._sections)
            self.text_var_iterator = None
            return self.edit_config != self.save_config
        return False

    def __get_text_variable(self) -> str:
        """
        The function implements an iterator from a list of variables
        associated with input fields to sequentially obtain their values
        """

        if self.text_var_iterator is None:
            self.text_var_iterator = iter(self.text_variables)
        return next(self.text_var_iterator).get()

    def show_about(self):
        """Displays a pop-up window with information about the program"""

        try:
            with open("./about.txt") as f:
                about = f.read()
        except FileNotFoundError:
            about = "Файл инструкции не найден"

        # messagebox.showinfo("О программе", about)
        about_dialog = AboutDialog(self, about)
        about_dialog.mainloop()


if __name__ == "__main__":
    config_editor = ConfigEditor()
    config_editor.mainloop()
