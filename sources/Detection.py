import numpy as np
import threading
import torch
import onnxruntime
from torchvision import transforms, models
from PIL import Image
from collections import namedtuple


DetectionData = namedtuple("DetectionData", ["frame",
                                             "detection_class",
                                             "gps"])


class Detection(threading.Thread):
    def __init__(self, main_app) -> None:
        super().__init__(name="model-detection-thread", daemon=False)
        self.app = main_app
        self._task = None
        self._result = None

        self.classes = ("Clear",
                        "Foregin objects",
                        "Trees")

        # self.data_transforms = transforms.Compose([
        #     transforms.Resize((1600, 1600)),
        #     transforms.CenterCrop(672),
        #     transforms.ToTensor(),
        #     transforms.Normalize(mean=[0.806], std=[0.225]),
        # ])

        # self.model = models.mobilenet_v3_large(weights=None,
        #                                        pretrained=True,
        #                                        progress=True)
        # self.model.classifier = torch.nn.Sequential(torch.nn.Linear(
        #     in_features=960,
        #     out_features=1280,
        #     bias=True),

        #     torch.nn.Hardswish(),
        #     torch.nn.Dropout(p=0.2,
        #                      inplace=True),
        #     torch.nn.Linear(in_features=1280,
        #                     out_features=len(self.classes),
        #                     bias=True))

        # device = torch.device("cpu")
        # self.model = self.model.to(device)
        # a = torch.load(self.app.config.model, "cpu")
        # self.model.load_state_dict(a)

        self.data_transforms = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
        ])

        self.model = onnxruntime.InferenceSession("./mobilenet_v3_small_3.onnx")

    @property
    def task(self) -> DetectionData:
        return self._task

    @task.setter
    def task(self, value: DetectionData) -> None:
        self._task = value

    @property
    def result(self) -> DetectionData:
        return self._result

    @result.setter
    def result(self, value: DetectionData) -> None:
        self._result = value

    def run(self) -> None:
        """
        This method detect frame inside separate thread
        """
        while self.app.is_run:
            if self.task is not None:
                frame, _, gps = self.task
                self.app.fps.start_measurement()
                detection_class = self.detect_class(frame)
                self.app.fps.end_measurement()
                self.app.fps.save_process_time()
                data = DetectionData(frame, detection_class, gps)
                self.result = data
                self.task = None

    def detect_class(self, frame: np.ndarray) -> str:
        data = Image.fromarray(frame)
        data_input = self.data_transforms(data)
        device = torch.device("cpu")
        data_input = data_input.to(device)
        data_input.unsqueeze_(0)

        # res = self.model(data_input)
        # res_class = res.argmin(dim=1)
        # res_class = res_class.data.cpu().numpy()
        input_name = self.model.get_inputs()[0].name
        output_name = self.model.get_outputs()[0].name
        res_class = np.argmax(self.model.run([output_name], {input_name: data_input.numpy()}))
        return self.classes[int(res_class)]
