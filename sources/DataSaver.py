from pathlib import Path
import pandas as pd
import numpy as np
from datetime import datetime
import cv2
import os
from sources.Detection import DetectionData


class DataSaver:
    def __init__(self, main_app) -> None:
        self.app = main_app
        self.frame_counter = 0
        self.data = []
        if self.app.config.save_dataframe or self.app.config.save_images:

            save_path = Path(self.app.config.save_path)
            if save_path.exists():
                print(f"The save folder '{save_path}'"
                      "was found successfully")
            else:
                save_path.mkdir(mode=0o666)
                print(f"The save folder '{save_path}'"
                      "has been created successfully")

            day_folder = save_path / datetime.today().strftime("%Y-%m-%d")
            if day_folder.exists():
                print(f"The date folder '{day_folder}'"
                      "was found successfully")
            else:
                day_folder.mkdir(mode=0o666)
                print(f"The date folder '{day_folder}'"
                      "has been created successfully")

            self.time_folder = day_folder / datetime.now().strftime("%X")
            self.time_folder.mkdir(mode=0o666)
            print(f"The time folder '{self.time_folder}'"
                  "has been created successfully")

    def add_detection_note(self, detection_result: DetectionData) -> None:
        """
        This method adds a new row to the data after
        receiving the detection result and saves the data

        :params numpy.ndarray data: The result from detection:
        """
        if self.app.config.save_images:
            path = self.save_photo(detection_result.frame)
            # path = asyncio.run(self.async_save_photo(detection_result.frame))
        else:
            path = "disabled"

        if self.app.config.save_images:
            new_row = (datetime.today().strftime("%Y-%m-%d"),
                       datetime.now().strftime("%X:%f")[:-3],
                       detection_result.detection_class,
                       detection_result.gps.get("lat", "n/a"),
                       detection_result.gps.get("lat", "n/a"),
                       path)

            self.data.append(new_row)

            if self.frame_counter % self.app.config.save_frequency == 0:
                self.save()

    def save(self) -> None:
        """
        This method convert data to dataframe and save as .csv file
        """
        if self.app.config.save_dataframe:
            columns = ("date",
                       "time",
                       "detection_result",
                       "gps-lat",
                       "gps-lon",
                       "filepath")
            df = pd.DataFrame(self.data, columns=columns)
            path = Path(self.time_folder) / "data.csv"
            df.to_csv(path,
                      mode="a",
                      header=(not path.exists()),
                      sep=';',
                      index=False)
            self.data.clear()
            os.chmod(path, 0o666)

    def save_photo(self, frame: np.ndarray) -> Path:
        """
        This method save frame disk

        :params numpy.ndarray frame: The frame from result detection
        """
        path = self.time_folder / f"{self.frame_counter:04}:.jpg"
        cv2.imwrite(str(path), frame)
        self.frame_counter += 1
        os.chmod(path, 0o666)
        return path
