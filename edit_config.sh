#!/bin/bash

# Активировать виртуальное окружение
source ./env/bin/activate

# Запустить Python-приложение
./env/bin/python3 ConfigEditor/config_editor.py
