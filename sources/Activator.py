from datetime import datetime
import hashlib
from tkinter import messagebox
import tkinter as tk


KEY = "9af15b336e6a9619928537df30b2e6a2376569fcf9d7e773eccede65606529a0"


class GUIActivator(tk.Tk):
    """
    Class for render gui for entry activation key
    """
    def __init__(self, main_app):
        super().__init__()
        self.app = main_app
        self.title("Активация")
        self.is_active = False
        self.text_var = tk.StringVar()

        img = tk.PhotoImage(file="./logo.png").subsample(4, 4)
        label = tk.Label(self, image=img)
        label.image = img
        label.pack(pady=10, padx=10)

        text = tk.Label(self, text="Введите ключ продукта")
        text.pack(pady=10, padx=10)

        self.entry = tk.Entry(self, width=30, textvariable=self.text_var)
        self.entry.pack(pady=10, padx=10)
        self.bind("<Key-Return>", self.check_subscription)

        self.button = tk.Button(self,
                                text="Активировать",
                                command=self.check_subscription)
        self.protocol("WM_DELETE_WINDOW", self.check_subscription)
        self.button.pack(pady=10)

    def subscription_is_active(self):
        """
        This method check activation key if subscription is over
        """
        deadline = datetime(2023, 12, 12)
        if datetime.today() > deadline:
            print("The subscription is not active")
            self.mainloop()
            return self.is_active
        print("The subscription is active")
        return True

    def check_subscription(self, event=None):
        """
        This is callback method handle button press,
        window close and enter key pressed events
        """
        key = self.text_var.get()
        hashkey = hashlib.sha256(key.encode())
        if hashkey.hexdigest() == KEY:
            self.is_active = True
            print("Activation key is valid")
            messagebox.showinfo(self, "Код активации найден")
        else:
            print("Activation key is invalid")
            messagebox.showerror(self, "Код активации не найден")
            self.app.is_run = False
        self.after(100, self.destroy)
