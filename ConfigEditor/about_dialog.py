from tkinter import Toplevel, Label


class AboutDialog(Toplevel):
    def __init__(self, parent, about_text):
        super().__init__(parent)
        self.title("О программе")
        self.geometry("640x480")
        self.resizable(False, False)
        self.transient(parent)
        self.grab_set()

        label = Label(self, text=about_text, wraplength=600, justify="left")
        label.pack(padx=10, pady=10)
