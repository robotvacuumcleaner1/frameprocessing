from configparser import ConfigParser
from typing import Union


class Config(ConfigParser):
    def __init__(self, file, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.read(file)

    @property
    def camera(self) -> Union[str, int]:
        camera = self.get("Camera", "source")
        if camera.isdigit() or camera == "-1":
            camera = int(camera)
        return camera

    @property
    def model(self) -> str:
        model = self.get("Model", "source")
        return model

    @property
    def show_detection_video(self) -> bool:
        show_detection_video = self.getboolean("Output",
                                               "show_detection_video")
        return show_detection_video

    @property
    def show_live_video(self) -> bool:
        show_live_video = self.getboolean("Output", "show_live_video")
        return show_live_video

    @property
    def terminal_output(self) -> bool:
        terminal_output = self.getboolean("Output", "terminal_output")
        return terminal_output

    @property
    def save_images(self) -> bool:
        save_images = self.getboolean("Save", "save_images")
        return save_images

    @property
    def save_dataframe(self) -> bool:
        save_dataframe = self.getboolean("Save", "save_dataframe")
        return save_dataframe

    @property
    def save_path(self) -> str:
        save_path = self.get("Save", "save_path")
        return save_path

    @property
    def save_frequency(self) -> str:
        save_frequency = self.getint("Save", "save_frequency")
        return save_frequency
