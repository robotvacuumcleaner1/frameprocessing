from tkinter import Frame, Canvas, Scrollbar
import tkinter as tk


class ScrollFrame(Frame):
    """
    This is a customized class that inherits
    from the Frame class in order to support
    scrolling of child objects
    """
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.canvas = Canvas(self)
        scrollbar = Scrollbar(self,
                              orient=tk.VERTICAL,
                              command=self.canvas.yview)
        self.scrollable_frame = Frame(self.canvas)
        self.scrollable_frame.bind("<Configure>", self.on_frame_configure)

        self.canvas.create_window((0, 0),
                                  window=self.scrollable_frame,
                                  anchor=tk.NW,
                                  tags="scrollable_frame")

        self.canvas.configure(yscrollcommand=scrollbar.set)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.canvas.bind("<Configure>", self.on_canvas_configure)
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

    def on_frame_configure(self, event):
        """
        Adjusts the size of the scrollable frame to match the canvas.
        """
        self.canvas.configure(scrollregion=self.canvas.bbox(tk.ALL))

    def on_canvas_configure(self, event):
        """
        Adjusts the size of the scrollable frame when canvas is resized.
        """
        self.canvas.itemconfig("scrollable_frame", width=event.width)

    def clear(self):
        for child in self.scrollable_frame.winfo_children():
            child.destroy()
